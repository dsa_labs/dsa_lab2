#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>

int rand(int seed) {
    long long prod = (22695477 * seed + 1) % (long long)pow(2, 32) + (long long)pow(2, 31);
    return prod % 250;
}

int main() {
    int x[250] = { 0 };
    std::ofstream results("results.txt");

    x[0] = 1;
    for (int i = 1; i < 40000; i++)
    {
        x[rand(i - 1)]++;
    }

    for (int i = 0; i < 250; i++)
    {
		printf("%d: %d\n", i, x[i]);
    	results << x[i] << std::endl;
	}
    results.close();

}